using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using LogTest;
using Xunit;

namespace dbTests
{
	//I have just realised that some of these tests will fail intermittently as the code works asynchronously
    //Need to redo.
    public class IntegrationTest1
    {
		private Random random = new Random();
       // Moq mock = new Mock<Directory>
        //Normally I prefer to use matchers instead of asserts as the messages are better.

		[Fact]
        public void DoesILogCreateAFolder()
        {
            Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));

            String logFolder = "/tmp/log/logtestx" + random.Next(500);
            Assert.False(Directory.Exists(logFolder));
            //Count all folders

            ILog testLog = new AsyncLog(logFolder);
            testLog.Write("test");
            testLog.StopWithFlush();
            //if we have more files then before it is working.
            Assert.True(Directory.EnumerateFiles(logFolder).Count() > 0);
            //Clean Up
            CleanUp(logFolder);
        }

        [Fact]
        public void DoesILogWriteAFile()
		{
			Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));
			String logFolder = "/tmp/log/logtestx" + random.Next(50);
			Directory.CreateDirectory(logFolder);
			//Count all folders
			int originalCount = Directory.EnumerateFiles(logFolder).Count();

			ILog testLog = new AsyncLog(logFolder);
			Trace.Write(originalCount);
			foreach (var item in Directory.EnumerateFiles(logFolder))
			{
				Trace.Write(item);
				Trace.Write(Environment.NewLine);
			}
			testLog.Write("test");
			testLog.StopWithFlush();
			int countAfterRun = (Directory.EnumerateFiles(logFolder)).Count();
			Trace.Write(countAfterRun);
			//if we have more files then before it is working.
			Assert.True(originalCount < countAfterRun);
			//Clean Up
			CleanUp(logFolder);
		}

		[Fact]
        public void DoesMidnightResultInANewFile(){
            //Mock the date time
        }
        [Fact]
        public void TestStopWithFlush(){
			Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));

			String logFolder = "/tmp/log/logtesty" + random.Next(500);
			ILog testLogger = new AsyncLog(logFolder);
			int expectedLines = 50;
			int currentLines = 0;
			for (int i = 0; i < expectedLines; i++){
				testLogger.Write("Log number: " + i);//This is a dirty way to convert int > string
			}
			testLogger.StopWithFlush();
			IEnumerable<String> files = Directory.EnumerateFiles(logFolder);
			foreach(String item in files){
				Trace.WriteLine(item);

			}
			Trace.WriteLine(logFolder);
            //for each file in the folder.
            foreach (String logFile in files)
			{
				int lines = 0;
                //Check the lines
				foreach (String item in File.ReadLines(logFile))
                {
                    //Check header is present
                    if (lines == 0)
                    {
                        Assert.True(item.Contains("Timestamp") && item.Contains("Data"));
						expectedLines++; //Increment to count headers.
                    }
                    else
                    {
                        Assert.Matches("", item.ToString());//contains a number
						currentLines++;
                    }

                    lines++;
                }
			}

			//Count logged lines accross all files.
			//Plus header lines.
			Assert.Equal(expectedLines, currentLines);

			CleanUp(logFolder);
        }
        [Fact]
        public void TestStopWithoutFlush()
		{
            //write 50 lines and ensure there are less then 50 lines in the file.
			String logFolder = "/tmp/log/logtestz" + random.Next(500);
            ILog testLogger = new AsyncLog(logFolder);
            int expectedLines = 50;
            int currentLines = 0;
            for (int i = 0; i < expectedLines; i++)
            {
                testLogger.Write("Log number: " + i);//This is a dirty way to convert int > string
            }
			testLogger.StopWithoutFlush();
            //for each file in the folder.
            foreach (String logFile in Directory.EnumerateFiles(logFolder))
            {
                int lines = 0;
                //Check the lines
                foreach (String item in File.ReadLines(logFile))
                {
                    //Check header is present
                    if (lines == 0)
                    {
                        Assert.True(item.Contains("Timestamp") && item.Contains("Data"));
                    }
                    else
                    {
						//Find a good regex
						Assert.Matches("*[0-5][0-9]*", item);//contains a number
                        currentLines++;
                    }

                    lines++;
                }
            }

            //Count logged lines accross all files.
            Assert.True(expectedLines != currentLines);

            CleanUp(logFolder);
		}


        //Removes all files from the folders
        //This should go in a finally.
        private void CleanUp(string logFolder)
        {
            Directory.Delete(logFolder, true);
        }
    }
}
