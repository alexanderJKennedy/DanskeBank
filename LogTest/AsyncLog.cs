﻿namespace LogTest
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Threading;

    public class AsyncLog : ILog
    {
        private Thread _runThread;
        //Remove lines
        private List<LogLine> _lines = new List<LogLine>();

		private List<LogLine> _handled = new List<LogLine>();

        private StreamWriter _writer; 

        private bool _exit;

        //Date the file was created
		private DateTime fileCreatedDate = DateTime.Now;

		private String LogFolder = Path.Combine(Path.GetFullPath(Directory.GetCurrentDirectory()), "logTestOutput");

		private String HeaderLine = "Timestamp".PadRight(25, ' ') + "\t" + "Data".PadRight(15, ' ') + "\t" + Environment.NewLine;

		public AsyncLog(String givenLogFolder)
		{
            if(!"".Equals(givenLogFolder)){
                LogFolder = givenLogFolder;
            }

			if (!Directory.Exists(LogFolder)){
				Directory.CreateDirectory(LogFolder);
				Console.WriteLine("No Folder making folder: " + LogFolder);
			}
                //create file
				this._writer = File.AppendText(workedFile());
                //write header
                this._writer.Write(HeaderLine);
                //set params for writer
				this._writer.AutoFlush = true;
				this._runThread = new Thread(this.MainLoop);
				this._runThread.Start();
			
	    }

        private bool _QuitWithFlush = false;


        private void MainLoop()
        {
            while (!this._exit)
            {



				List<LogLine> workinglines = this._lines;

				if (workinglines.Count > 0)
                 {
                    
					foreach (LogLine logLine in workinglines) {
						
                        if (!this._exit || this._QuitWithFlush)
                        {
                            _handled.Add(logLine);//add for removing later

							StringBuilder stringBuilder = new StringBuilder();
							if ((DateTime.Now - fileCreatedDate).Days != 0)
                            {
								this._writer = File.AppendText(workedFile());
								this._writer.Write(HeaderLine);

                                this._writer.Write(stringBuilder.ToString());
                                this._writer.AutoFlush = true;
                            }
                            //include timezone
							stringBuilder.Append(logLine.Timestamp.ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss:fff \"UTC\"zzz"));
                            stringBuilder.Append("\t");
                            stringBuilder.Append(logLine.LineText());
                            stringBuilder.Append("\t");
                            stringBuilder.Append(Environment.NewLine);

							Console.WriteLine( "writing to file : " + stringBuilder.ToString());
                            this._writer.Write(stringBuilder.ToString());
                        }
        			} 
				} else {
                        //Dont sit spinning with no inputs.
                        //Theres possibly a latency but it saves cpu and power.
                        Thread.Sleep(20);
                    }

                if (this._QuitWithFlush == true && this._lines.Count == 0) {
                    this._exit = true;
                }


              for (int y = 0; y < _handled.Count; y++)
                {
                    this._lines.Remove(_handled[y]);   
                }
            }
        }

        public void StopWithoutFlush()
        {
            this._exit = true;
        }

        public void StopWithFlush()
        {
            this._QuitWithFlush = true;
        }

        public void Write(string text)
        {
            this._lines.Add(new LogLine() { Text = text, Timestamp = DateTime.Now });
        }
        
//Generates the file name under working.
        private string workedFile(){
			fileCreatedDate = DateTime.Now;
			return Path.Combine(LogFolder, fileCreatedDate.ToString("yyyyMMdd_HH:mm:ss_fff")) + ".log";
        }

    }
}